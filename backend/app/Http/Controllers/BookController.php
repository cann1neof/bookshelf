<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Book;

class BookController extends Controller
{
    public function add(Request $request){
        $book = new Book;
        
        $book->title = $request->title;
        $book->author = $request->author;
        $book->availability = true;

        $book->save();

        return response($book, 201);
    }

    public function all(){
        $booksArr = Book::all();
        return $booksArr;
    }

    public function delete($id){
        Book::destroy($id);
        return response('success', 200);
    }

    public function changeAvailabilty($id){
        $book = Book::find($id);
        $book->availability = !$book->availability;
        $book->save();
        return response('success', 200);
    }

}
